<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<!-- Include jQuery -->
<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<!-- <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.ocean.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>

<div class="row">
    <div class="col-md-4">
        <table class="table table-bordered table-striped">
            <thead>
                <tr class="bg-secondary">
                    <th>KDN/JABATAN</th>
                    <th>JUMLAH</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($rows as $row): ?>
                <tr>
                    <td><?= $row->keterangan_jabatan ?></td>
                    <td><?= $row->bil ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-md-8">
        <div id="chart-container">FusionCharts will render here</div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">Senarai Projek</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="senarai-projek">
        
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    //STEP 2 - Chart Data
    const chartData = [
        <?php
        foreach($rows as $row) {
            echo "{label: '{$row->keterangan_jabatan}', value: '{$row->bil}', kod: '{$row->kod_jabatan}'},";
        }
        ?>
    ];
    //STEP 3 - Chart Configurations
    const chartConfigs = {
        type: "bar3d",
        width: "700",
        height: "500",
        dataFormat: "json",
        dataSource: {
            // Chart Configuration
            "chart": {
                "caption": "STATUS DALAM PELAKSANAAN BAGI PROJEK KDN/JABATAN",
                "xAxisName": "JABATAN",
                "yAxisName": "BIL PROJEK",
                "theme": "ocean",
            },
            // Chart Data
            "data": chartData
        },
        events: {
            dataPlotClick: function(ev, props) {
                let data = chartData[props.index];
                //alert(data.kod);
                $('#senarai-projek').load('<?= base_url('/projek4-data') ?>/' + data.kod);
                const myModal = new bootstrap.Modal(document.getElementById('myModal'), {});
                myModal.show();
                console.log(props);
            }
        }
    }
    // Create a chart container
    $('document').ready(function () {
        $("#chart-container").insertFusionCharts(chartConfigs);
    });
</script>
        
<?= $this->endSection() ?>