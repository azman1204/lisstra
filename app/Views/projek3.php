<?= $this->extend('master') ?>
<?= $this->section('content') ?>
<!-- Include jQuery -->
<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<!-- <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>
</head>
<body>

<!-- Modal -->
<div class="modal fade" id="myModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">Senarai Projek</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body" id="senarai-projek">
        
      </div>
    </div>
  </div>
</div>

<div id="chart-container">FusionCharts will render here</div>
<table class="table table-bordered">
    <thead>
        <tr>
            <th></th>
            <?php 
            foreach($jabatan as $jab) {
                echo "<th>{$jab->keterangan_jabatan}</th>";
            }
            ?>
        </tr>
    </thead>
    <tbody>
        <?php 
        $str = '';
        foreach($status as $name => $st) {
            $str .= "<tr><td>$name</td>";
            foreach ($st as $s) {
                $str .= "<td>$s</td>";
            }
            $str .= "</tr>";
        }
        echo $str;
        ?>
    </tbody>
</table>

<script type="text/javascript">
let jabatan = [
    <?php
    foreach ($jabatan as $jab) {
        echo "{label: '$jab->keterangan_jabatan', kod_jabatan: '$jab->kod_jabatan'},";
    }
    ?>
];

let kategori = [
    <?php
    foreach($kategori as $kat) {
        echo "'$kat->kod_kategori',";
    }
    ?>
];

let status = [
    <?php
    foreach($status as $name => $st) {
        $str = '';
        foreach ($st as $s) {
            $str .= "{value: $s},";
        }
        echo "{ seriesname: '$name', data: [$str] },";
    }
    ?>
];

$("#chart-container").insertFusionCharts({
    type: "stackedbar2d",
    width: "100%",
    height: "400",
    dataFormat: "json",
    events: {
        dataPlotClick: function(ev, props) {
            let kod_jabatan = jabatan[props.dataIndex].kod_jabatan;
            let kod_kategori = kategori[props.datasetIndex];
            $('#senarai-projek').load('<?= base_url('/projek3-data') ?>/' + kod_jabatan + '/' + kod_kategori);
            const myModal = new bootstrap.Modal(document.getElementById('myModal'), {});
            myModal.show();
            console.log(props.dataIndex, props.datasetIndex);
        }
    },
    dataSource: {
        chart: {
            caption: "Status kategori Bagi Projek KDN / Jabatan",
            captionAlignment: "left",
            placevaluesinside: "1",
            showvalues: "1",
            plottooltext: "<b>$dataValue</b>  $seriesName visitors",
            theme: "fusion"
        },
        categories: [
        {
            category: jabatan
        }
        ],
        dataset: status
    }
});
</script>

<?= $this->endSection() ?>