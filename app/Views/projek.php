<?= $this->extend('master') ?>
<?= $this->section('content') ?>
<!-- Include jQuery -->
<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<!-- <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.ocean.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>
</head>
<body>
<form action="/projek" method="post">
    <?= csrf_field() ?>
    Tahun Dari: <input type="text" name="tahun_dari">
    Tahun Hingga : <input type="text" name="tahun_hingga">
    <input type="submit" value="Cari">
</form>

<div id="chart-container">FusionCharts will render here</div>
<script type="text/javascript">
    //STEP 2 - Chart Data
    const chartData = [
        <?php
        foreach($rows as $row) {
            $label = $row->keterangan_jabatan;
            $value = $row->bil;
            echo "{label: '$label', value: '$value'},";
        }
        ?>
    ]
    //STEP 3 - Chart Configurations
    const chartConfigs = {
        type: "column2d",
        width: "700",
        height: "400",
        dataFormat: "json",
        dataSource: {
            // Chart Configuration
            "chart": {
                "caption": "Bil Projek Mengikut Agensi",
                "xAxisName": "Agensi",
                "yAxisName": "Bil Projek",
                "theme": "ocean"
            },
            // Chart Data
            "data": chartData
        },
        events: {
            dataPlotClick: function(e) {
                var index = e.data.dataIndex;
                alert((chartData[index]).label);
            }
        }
    }
    // Create a chart container
    $('document').ready(function () {
        $("#chart-container").insertFusionCharts(chartConfigs);
    });
</script>

<?= $this->endSection() ?>