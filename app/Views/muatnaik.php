<?php
$this->extend('master');
$this->section('content');
?>
<link rel="stylesheet" href="https://unpkg.com/dropzone@5/dist/min/dropzone.min.css" type="text/css" />
<script src="https://unpkg.com/dropzone@5/dist/min/dropzone.min.js"></script>

<form id="my-dropzone" method="post" 
class="dropzone" action="/upload/ali" enctype="multipart/form-data">
    <!-- <input type="file" name="doc">
    <input type="submit" value="Muatnaik Sekarang"> -->
</form>

<script>
Dropzone.options.myDropzone = {
    paramName: "doc"
};
</script>
<?php
$this->endSection();