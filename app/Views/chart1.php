<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<!-- Include jQuery -->
<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<!-- <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.ocean.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>

<div id="chart-container">FusionCharts will render here</div>

<!-- Modal -->
<div class="modal fade" id="myModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h1 class="modal-title fs-5" id="staticBackdropLabel">Modal title</h1>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Understood</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
    //STEP 2 - Chart Data
    const chartData = [
        {
            label: "Venezuela",
            value: "290"
        }, {
            label: "Saudi",
            value: "260"
        }, {
            label: "Canada",
            value: "180"
        }, {
            label: "Iran",
            value: "140"
        }, {
            label: "Russia",
            value: "115"
        }, {
            label: "UAE",
            value: "100"
        }, {
            label: "US",
            value: "30"
        }, {
            label: "China",
            value: "30"
        }
    ];
    //STEP 3 - Chart Configurations
    const chartConfigs = {
        type: "column3d",
        width: "700",
        height: "400",
        dataFormat: "json",
        dataSource: {
            // Chart Configuration
            "chart": {
                "caption": "Countries With Most Oil Reserves [2017-18]",
                "subCaption": "In MMbbl = One Million barrels",
                "xAxisName": "Country",
                "yAxisName": "Reserves (MMbbl)",
                "numberSuffix": "K",
                "theme": "ocean",
            },
            // Chart Data
            "data": chartData
        },
        events: {
            dataPlotClick: function(ev, props) {
                var index = props.dataIndex;
                //alert(chartData[index].label);
                const myModal = new bootstrap.Modal('#myModal', {
                    keyboard: false
                });
                myModal.toggle();
            }
        }
    }
    // Create a chart container
    $('document').ready(function () {
        $("#chart-container").insertFusionCharts(chartConfigs);
    });
</script>
        
<?= $this->endSection() ?>