<table class="table table-bordered table-primary table-striped">
    <thead class="bg-secondary">
        <tr>
            <th>Bil</th>
            <th>Nama Projek</th>
            <th>Tarikh Mula</th>
            <th>Tarikh Tamat</th>
            <th>Kos</th>
        </tr>
    </thead>
    <tbody>
        <?php 
        $bil = 1;
        foreach($rows as $row) : ?>
        <tr>
            <td><?= $bil++ ?></td>
            <td><?= $row->tajuk ?></td>
            <td><?= $row->tarikh_mula ?></td>
            <td><?= $row->tarikh_tamat ?></td>
            <td><?= $row->kos ?></td>
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>