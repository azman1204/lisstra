<?php
$this->extend('master');
$this->section('title');echo "Mesyuarat JPICT";$this->endSection();
$this->section('content');
?>
<form method="post" action="<?= base_url('/jpict-cari') ?>" class="mb-2">
    <div class="row mb-2">
        <div class="col-md-1">Tahun</div>
        <div class="col-md-2">
            <select name="tahun" class="form-control" id="tahun">
                <option value="0">-- Sila Pilih --</option>
                <?php foreach($tahun as $t) : ?>
                <option><?= $t->tahun ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-1">Mesyuarat JPICT</div>
        <div class="col-md-2" id="mesyuarat"></div>
    </div>
    <div class="row">
        <div class="col-md-1">Jabatan</div>
        <div class="col-md-5">
            <select name="jabatan" class="form-control">
                <option value="0">-- Sila Pilih --</option>
                <?php foreach($jabatan as $j) : ?>
                <option value="<?= $j->kod_jabatan ?>">
                    <?= $j->keterangan_jabatan ?>
                </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-1"><input type="submit" class="btn btn-primary"></div>
    </div>
</form>

<div class="btn-toolbar mb-2 float-end">
    <div class="btn-group me-2">
        <a href="<?= base_url('/jpict-pdf') ?>" target="_window" class="btn btn-sm btn-outline-secondary">Muat turun PDF</a>
        <a href="<?= base_url('/jpict-excel') ?>" class="btn btn-sm btn-outline-secondary">Muat turun Excel</a>
    </div>
</div>
<?php
include 'senarai.php';
?>

<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script>
$(function() {
    $('#tahun').change(function() {
        let tahun = $(this).val();
        $('#mesyuarat').load('<?= base_url('/jpict-mesyuarat') ?>/' + tahun);
    });
});
</script>
<?php
$this->endSection();