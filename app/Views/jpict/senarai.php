<?php if ($mode == 'pdf') : ?>
<h2>Laporan JPICT</h2>
<?php endif; ?>

<?php foreach ($rs as $mesy): ?>
    <?php 
    $info = current($mesy); 
    if (! $info) continue;
    ?>
    <h4>
        <?= $info->mesyuarat ?> 
        (<?= $info->tarikh ?> / <?= $info->hari ?> <?= $info->masa ?>) 
        - <?= $info->pengerusi ?>
    </h4>

    <table class="table table-bordered table-striped">
        <tr>
            <td>Bil</td>
            <td>Perkara</td>
            <td>Tempoh</td>
            <td>Kos</td>
            <td>Jabatan</td>
            <td>Sumber Peruntukan</td>
            <td>Kaedah Perolehan</td>
            <td>Bil KP</td>
            <td>Catatan</td>
        </tr>
        <?php foreach($mesy as $jpict): ?>
        <tr>
            <td><?= $jpict->kod_mesyJPICT ?></td>
            <td><?= $jpict->tajuk ?></td>
            <td><?= $jpict->tempoh ?></td>
            <td><?= $jpict->kos ?></td>
            <td><?= $jpict->kod_jabatan ?></td>
            <td><?= $jpict->sumber_peruntukan ?></td>
            <td><?= $jpict->kaedah_perolehan ?></td>
            <td><?= $jpict->bil_kp ?></td>
            <td><?= $jpict->catatan ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endforeach; ?>

<?php if ($mode == 'pdf') : ?>
<style>
    table {
        border:2px solid #999;
        border-spacing: 1px;
    }

    td {
        border:1px solid #ccc;
    }
</style>
<?php endif; ?>