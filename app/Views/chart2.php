<?= $this->extend('master') ?>
<?= $this->section('content') ?>
<!-- Include jQuery -->
<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<!-- <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>
<div id="chart-container">FusionCharts will render here</div>
<script type="text/javascript">
$("#chart-container").insertFusionCharts({
    type: "doughnut3d",
    width: "100%",
    height: "400",
    dataFormat: "json",
    dataSource: {
        chart: {
            caption: "Market Share of Web Servers",
            plottooltext: "<b>$percentValue</b> of web servers run on $label servers",
            showlegend: "1",
            showpercentvalues: "1",
            legendposition: "bottom",
            usedataplotcolorforlabels: "1",
            theme: "fusion"
        },
        data: [
            {
                label: "Apache",
                value: "32647479"
            },
            {
                label: "Microsoft",
                value: "22100932"
            },
            {
                label: "Zeus",
                value: "14376"
            },
            {
                label: "Other",
                value: "18674221"
            }
        ]
    },
    events: {
        dataPlotClick: function(ev, props) {
            console.log(props);
        }
    }
});
</script>
        
<?= $this->endSection() ?>