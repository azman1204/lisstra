<?= $this->extend('master') ?>
<?= $this->section('content') ?>
<!-- Include jQuery -->
<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<!-- <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>

<div id="chart-container">FusionCharts will render here</div>
<script type="text/javascript">
$("#chart-container").insertFusionCharts({
    type: "stackedbar2d",
    width: "50%",
    height: "400",
    dataFormat: "json",
    dataSource: {
    chart: {
        caption: "Split of visitors by Channels & Gender",
        captionAlignment: "left",
        placevaluesinside: "1",
        showvalues: "0",
        plottooltext: "<b>$dataValue</b>  $seriesName visitors",
        theme: "fusion"
    },
    categories: [
    {
        category: [
        {
            label: "Organic"
        },
        {
            label: "Offline Stores"
        },
        {
            label: "Email Campaigns"
        },
        {
            label: "Social Events"
        },
        {
            label: "Paid Channels"
        }
        ]
    }
    ],
    dataset: [
    {
        seriesname: "Male",
        data: [
        {
            value: "17000"
        },
        {
            value: "19500"
        },
        {
            value: "12500"
        },
        {
            value: "14500"
        },
        {
            value: "17500"
        }
        ]
    },
    {
        seriesname: "Female",
        data: [
        {
            value: "25400"
        },
        {
            value: "29800"
        },
        {
            value: "21800"
        },
        {
            value: "19500"
        },
        {
            value: "21200"
        }
        ]
    }
    ]
}
});
</script>
        
<?= $this->endSection() ?>