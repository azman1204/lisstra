<?php if ($mode == 'pdf') : ?>
<h2>Laporan Jawatankuasa Teknikal..</h2>
<?php endif; ?>

<table class="table table-bordered table-striped">
    <tr>
        <td>Bil</td>
        <td>Tarikh</td>
        <td>Pengerusi</td>
        <td>No</td>
        <td>Jabatan</td>
        <td>Nama Ringkas Projek</td>
        <td>Kos</td>
        <td>Tempoh</td>
        <td>Sumber Peruntukan</td>
        <td>Ulasan JTI</td>
        <td>Tarikh Terima Permohonan</td>
    </tr>
    <?php foreach($rows as $jti): ?>
    <tr>
        <td><?= $jti->bil ?></td>
        <td><?= $jti->tarikh ?></td>
        <td><?= $jti->pengerusi ?></td>
        <td><?= $jti->no ?></td>
        <td><?= $jti->keterangan_jabatan ?></td>
        <td><?= $jti->tajuk ?></td>
        <td><?= $jti->kos ?></td>
        <td><?= $jti->tempoh ?></td>
        <td><?= $jti->sumber_peruntukan ?></td>
        <td><?= $jti->ulasan_jti ?></td>
        <td><?= $jti->tarikh_terima_permohonan ?></td>
    </tr>
    <?php endforeach; ?>
</table>

<?php if ($mode == 'pdf') : ?>
<style>
    table {
        border:2px solid #999;
        border-spacing: 1px;
    }

    td {
        border:1px solid #ccc;
    }
</style>
<?php endif; ?>