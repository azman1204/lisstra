<?php
$this->extend('master');
$this->section('content');
include 'cari_jti.php';
?>

<div class="btn-toolbar mb-2 float-end">
    <div class="btn-group me-2">
        <a href="<?= base_url('/report-jti-pdf') ?>" target="_window" class="btn btn-sm btn-outline-secondary">Muat turun PDF</a>
        <a href="<?= base_url('/report-jti-excel') ?>" class="btn btn-sm btn-outline-secondary">Muat turun Excel</a>
    </div>
</div>

<?php
include 'report_jti.php';
$this->endSection();