<?= $this->extend('master') ?>
<?= $this->section('content') ?>

<script type="text/javascript" src="<?= base_url('/js/fusioncharts.js') ?>"></script>
<script type="text/javascript" src="<?= base_url('/js/themes/fusioncharts.theme.fusion.js') ?>"></script>

<center>
    <div id="chart-container">Chart will render here!</div>
</center>

<?php
include("fusioncharts.php");
// An array of hash objects which stores data
$arrChartData = array(
    ["Venezuela", "290"],
    ["Saudi", "260"],
    ["Canada", "180"],
    ["Iran", "140"],
    ["Russia", "115"],
    ["UAE", "100"],
    ["US", "30"],
    ["China", "30"]
);

$arrLabelValueData = array();
// Pushing labels and values
for($i = 0; $i < count($arrChartData); $i++) {
    array_push($arrLabelValueData, array(
        "label" => $arrChartData[$i][0], "value" => $arrChartData[$i][1]
    ));
}

// Chart Configuration stored in Associative Array
$arrChartConfig = array(
    "chart" => array(
        "caption" => "Countries With Most Oil Reserves [2017-18]",
        "subCaption" => "In MMbbl = One Million barrels",
        "xAxisName" => "Country",
        "yAxisName" => "Reserves (MMbbl)",
        "numberSuffix" => "K",
        "theme" => "fusion"
    )
);

$arrChartConfig["data"] = $arrLabelValueData;
// JSON Encode the data to retrieve the string containing the JSON representation of the data in the array.
$jsonEncodedData = json_encode($arrChartConfig);

$Chart = new FusionCharts("column2d", "MyFirstChart" , "700", "400", "chart-container", "json", $jsonEncodedData);

// Render the chart
$Chart->render();
$this->endSection();
?>