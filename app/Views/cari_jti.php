<?php
$this->extend('master');
$this->section('content');
?>
<form method="post" action="<?= base_url('/report-jti') ?>" class="mb-2">
    <div class="row mb-2">
        <div class="col-md-1">Tahun</div>
        <div class="col-md-2">
            <select name="tahun" class="form-control">
                <option value="0">-- Sila Pilih --</option>
                <?php foreach($tahun as $t) : ?>
                <option><?= $t->tahun ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-1">Bulan</div>
        <div class="col-md-2">
            <select name="bulan" class="form-control">
                <option value="0">-- Sila Pilih --</option>
                <?php for($i=1; $i<=12; $i++) : ?>
                <option><?= $i ?></option>
                <?php endfor; ?>
            </select>
        </div>
    </div>
    <div class="row">
        <div class="col-md-1">Jabatan</div>
        <div class="col-md-5">
            <select name="jabatan" class="form-control">
                <option value="0">-- Sila Pilih --</option>
                <?php foreach($jabatan as $j) : ?>
                <option value="<?= $j->kod_jabatan ?>">
                    <?= $j->keterangan_jabatan ?>
                </option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="col-md-1"><input type="submit" class="btn btn-primary"></div>
    </div>
</form>
<?php
$this->endSection();