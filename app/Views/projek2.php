<?= $this->extend('master') ?>
<?= $this->section('content') ?>
<!-- Include jQuery -->
<script type="text/javascript" src=" https://code.jquery.com/jquery-3.3.1.min.js"></script>
<!-- Include fusioncharts core library file -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/fusioncharts.js"></script>
<!-- Include fusion theme file -->
<!-- <script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.fusion.js"></script> -->
<script type="text/javascript" src=" https://cdn.fusioncharts.com/fusioncharts/latest/themes/fusioncharts.theme.ocean.js"></script>
<!-- Include fusioncharts jquery plugin -->
<script type="text/javascript" src=" https://rawgit.com/fusioncharts/fusioncharts-jquery-plugin/develop/dist/fusioncharts.jqueryplugin.min.js"></script>
</head>
<body>

<div id="chart-container">FusionCharts will render here</div>
<script type="text/javascript">
    //STEP 2 - Chart Data
    const chartData = [
        <?php
        foreach($rows as $row) {
            $label = $row->keterangan_status;
            $value = $row->bil;
            echo "{label: '$label', value: '$value'},";
        }
        ?>
    ]
    //STEP 3 - Chart Configurations
    const chartConfigs = {
        type: "doughnut3d",
        width: "100%",
        height: "400",
        dataFormat: "json",
        dataSource: {
        chart: {
            caption: "Status Terkini Bagi Projek KDN / Jabatan",
            plottooltext: "<b>$percentValue</b> $label projek",
            showlegend: "1",
            showpercentvalues: "1",
            legendposition: "bottom",
            usedataplotcolorforlabels: "1",
            theme: "ocean"
        },
        data: chartData
    }
    }
    // Create a chart container
    $('document').ready(function () {
        $("#chart-container").insertFusionCharts(chartConfigs);
    });
</script>

<?= $this->endSection() ?>