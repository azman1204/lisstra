<?php
namespace App\Controllers;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
class JPICTController extends BaseController {
    public function cari() {
        $db = db_connect();

        $year = request()->getPost('tahun');
        $kod_mesyuarat = request()->getPost('kod_mesyuarat');
        $dept = request()->getPost('jabatan');

        $sql = "SELECT DISTINCT YEAR(tarikh) AS tahun
                FROM lt_mesyuaratjpict";
        $query = $db->query($sql);
        $tahun = $query->getResult();

        $sql = "SELECT * FROM lt_jabatan";
        $query = $db->query($sql);
        $jabatan = $query->getResult();

        $where = '';
        
        if ($year !== '0' && ! empty($year)) {
            $where .= " AND YEAR(tarikh) = $year";
        }

        $bil_mesy = [];
        if ($kod_mesyuarat !== '0' && ! empty($kod_mesyuarat)) {
            // user pilih spesifik mesyuarat
            $bil_mesy[] = $kod_mesyuarat;
        } else {
            // pilih semua mesyuarat
            $sql = "SELECT DISTINCT kod_mesyJPICT FROM lt_mesyuaratJPICT 
                    WHERE 1 = 1 $where";
            $query = $db->query($sql);
            $rows = $query->getResult();
            foreach ($rows as $row) {
                $bil_mesy[] = $row->kod_mesyJPICT;
            }
        }

        if ($dept !== '0' && ! empty($dept)) {
            $where .= " AND a.kod_jabatan = $dept";
        }

        $rs = []; // result set by bilangan mesyuarat
        foreach($bil_mesy as $bil) {
            $sql = "SELECT * 
                    FROM tr_jpict a, lt_mesyuaratjpict b
                    WHERE a.kod_mesyJPICT = b.kod_mesyJPICT $where
                    AND a.kod_mesyJPICT = '$bil'
                    ORDER BY a.kod_mesyJPICT";
            
            $query = $db->query($sql);
            $rs[$bil] = $query->getResult();
        }
        //print_r($rs);
        session()->set(['rs' => $rs]);
        $mode = 'html';
        return view('jpict/cari', compact('tahun', 'jabatan', 'rs', 'mode'));
    }

    public function mesyuarat($tahun = 2021) {
        $db = db_connect();
        $sql = "SELECT b.mesyuarat, b.kod_mesyJPICT 
                FROM lt_mesyuaratjpict b
                WHERE YEAR(b.tarikh) = $tahun";
        $query = $db->query($sql);
        $rows = $query->getResult();
        $str = "<select name='kod_mesyuarat' class='form-control'>";
        $str .= "<option value='0'>-- Semua Bilangan Mesyuarat --</option>";
        foreach($rows as $row) {
            $str .= "<option value='{$row->kod_mesyJPICT}'>{$row->mesyuarat}</option>";
        }
        $str .= "</select>";
        return $str;
    }

    public function genPdf() {
        $rs = session()->get('rs');
        $mode = 'pdf';
        $html = view('jpict/senarai', compact('rs', 'mode'));
        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
    }

    public function genExcel() {
        $rs = session()->get('rs');
        $spreadsheet = new Spreadsheet();
        foreach ($rs as $bil => $rows) {
            $myWorkSheet = new Worksheet($spreadsheet, "MESY $bil");
            $spreadsheet->addSheet($myWorkSheet, 0);
            //$activeWorksheet = $spreadsheet->getActiveSheet("MESY $bil");
            $activeWorksheet = $spreadsheet->getSheetByName("MESY $bil");
            $activeWorksheet->setCellValue("A1", "Bil");
            $activeWorksheet->setCellValue("B1", "Perkara");
            $activeWorksheet->setCellValue("C1", "Tempoh");
            $activeWorksheet->setCellValue("D1", "Kos");
            $activeWorksheet->setCellValue("E1", "Agensi / Jabatan");
            $activeWorksheet->setCellValue("F1", "Sumber Peruntukan");
            $activeWorksheet->setCellValue("G1", "Kaedah Perolehan");
            $activeWorksheet->setCellValue("H1", "Bil KP");
            $activeWorksheet->setCellValue("I1", "Catatan");
            $i = 2;
            foreach($rows as $row) {
                $activeWorksheet->setCellValue("A$i", $row->kod_mesyJPICT);
                $activeWorksheet->setCellValue("B$i", $row->tajuk);
                $activeWorksheet->setCellValue("C$i", $row->tempoh);
                $activeWorksheet->setCellValue("D$i", $row->kos);
                $activeWorksheet->setCellValue("E$i", $row->kod_jabatan);
                $activeWorksheet->setCellValue("F$i", $row->sumber_peruntukan);
                $activeWorksheet->setCellValue("G$i", $row->kaedah_perolehan);
                $activeWorksheet->setCellValue("H$i", $row->bil_kp);
                $activeWorksheet->setCellValue("I$i", $row->catatan);
                $i++;
            }
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('jpict.xlsx');
        return redirect()->to(base_url('/jpict.xlsx'));
    }
}