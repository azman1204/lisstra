<?php
namespace App\Controllers;

class TestController extends BaseController {
    public function index() {
        return view('test/index');
    }

    public function upload() {
        $input = $this->validate([
            'doc' => [
                'uploaded[doc]',
                'mime_in[doc,image/jpg,image/jpeg,image/png]',
                'max_size[doc,1024]',
            ]
        ]);

        if (!$input) {
            print_r('Choose a valid file');
        } else {
            $file = $this->request->getFile('doc');
            $name = $file->getRandomName();
            echo $name;
            $file->move(WRITEPATH . 'uploads');
        }
    }
}