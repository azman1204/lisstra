<?php
namespace App\Controllers;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportController extends BaseController {
    public function html2pdf() {
        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml(view('pdf_view'));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dompdf_out.pdf", 
        array("Attachment" => false));
    }

    public function jti() {
        $db = db_connect();
        $year = $this->request->getPost('tahun');
        $bulan = $this->request->getPost('bulan');
        $dept = $this->request->getPost('jabatan');

        $sql = "SELECT DISTINCT YEAR(tarikh) AS tahun
                FROM tr_jti";
        $query = $db->query($sql);
        $tahun = $query->getResult();

        $sql = "SELECT * FROM lt_jabatan";
        $query = $db->query($sql);
        $jabatan = $query->getResult();

        $sql = "SELECT * 
                FROM tr_jti jti, lt_jabatan j
                WHERE jti.kod_jabatan = j.kod_jabatan";
        
        if ($year !== '0' && ! empty($year)) {
            $sql .= " AND YEAR(tarikh) = $year";
        }

        if ($bulan !== '0' && ! empty($bulan)) {
            $sql .= " AND MONTH(tarikh) = $bulan";
        }

        if ($dept !== '0' && ! empty($dept)) {
            $sql .= " AND jti.kod_jabatan = '$dept'";
        }

        session()->set(['sql' => $sql]);

        $query = $db->query($sql);
        $rows = $query->getResult();
        $mode = 'html';
        return view('report_jti_html', compact('rows', 'mode', 'tahun', 'jabatan'));
    }

    public function jtiPdf() {
        $db = db_connect();
        $query = $db->query(session()->get('sql'));
        $rows = $query->getResult();
        $mode = 'pdf';
        $html = view('report_jti', compact('rows', 'mode'));
        $dompdf = new \Dompdf\Dompdf(); 
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream("dompdf_out.pdf", 
        array("Attachment" => false));
    }

    public function jtiExcel() {
        $db = db_connect();
        $query = $db->query(session()->get('sql'));
        $rows = $query->getResult();
        $spreadsheet = new Spreadsheet();
        $activeWorksheet = $spreadsheet->getActiveSheet();
        $activeWorksheet->setCellValue("A1", "Bil");
        $activeWorksheet->setCellValue("B1", "Tarikh Mula");
        $activeWorksheet->setCellValue("C1", "Pengerusi");
        $activeWorksheet->setCellValue("D1", "No");
        $activeWorksheet->setCellValue("E1", "Agensi / Jabatan");
        $activeWorksheet->setCellValue("F1", "Tajuk Ringkas");
        $activeWorksheet->setCellValue("G1", "Kos");
        $activeWorksheet->setCellValue("H1", "Tempoh");
        $activeWorksheet->setCellValue("I1", "Sumber Peruntukan");
        $activeWorksheet->setCellValue("J1", "Ulasan");
        $activeWorksheet->setCellValue("K1", "Tarikh Terima Permohonan");
        $i = 2;
        foreach($rows as $row) {
            $activeWorksheet->setCellValue("A$i", $row->bil);
            $activeWorksheet->setCellValue("B$i", $row->tarikh);
            $activeWorksheet->setCellValue("C$i", $row->pengerusi);
            $activeWorksheet->setCellValue("D$i", $row->no);
            $activeWorksheet->setCellValue("E$i", $row->keterangan_jabatan);
            $activeWorksheet->setCellValue("F$i", $row->tajuk);
            $activeWorksheet->setCellValue("G$i", $row->kos);
            $activeWorksheet->setCellValue("H$i", $row->tempoh);
            $activeWorksheet->setCellValue("I$i", $row->sumber_peruntukan);
            $activeWorksheet->setCellValue("J$i", $row->ulasan_jti);
            $activeWorksheet->setCellValue("K$i", $row->tarikh_terima_permohonan);
            $i++;
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('jti.xlsx');
        return redirect()->to(base_url('/jti.xlsx'));
    }

    public function helloExcel() {
        $spreadsheet = new Spreadsheet();
        $activeWorksheet = $spreadsheet->getActiveSheet();

        for($i=1; $i<10; $i++) {
            $activeWorksheet->setCellValue("A$i", 'Hello World !');
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('hello.xlsx');
        //echo base_url('/hello world.xlsx');
        return redirect()->to(base_url('/hello.xlsx'));
    }
}