<?php

namespace App\Controllers;

use App\Controllers\BaseController;

class UploadController extends BaseController
{
    public function index() {
        return view('muatnaik');
    }

    public function store($folder) {
        $img = $this->request->getFile('doc');
        //$name =  $img->getName(); // ori name
        $name = $img->getRandomName();
        $created_at = date('Y-m-d H:i:s');

        $db = db_connect();
        $sql = "INSERT INTO attachment(name, created_at)
                VALUES('$name', '$created_at')";
        $db->query($sql);
        // WRITEPATH = c:\laragon\www\lisstra\writable\muatnaik
        $img->move(WRITEPATH . "muatnaik/$folder/", $name, true);
    }

    public function getFile() {
        // check user permission
        //$content = file_get_contents(WRITEPATH . 'muatnaik/azmn.png');
        $path = WRITEPATH . 'muatnaik/azmn.png';
        return $this->response->download($path, null);
    }
}
