<?php
namespace App\Controllers;

class SampleController extends BaseController {
    public function hello() {
        return view('hello'); // views/hello.php
    }

    public function chart1() {
        return view('chart1');
    }

    public function chart2() {
        return view('chart2');
    }

    public function chart3() {
        return view('chart3');
    }

    public function chart4() {
        return view('chart4');
    }

    public function db1() {
        $db = db_connect();
        $query = $db->query("SELECT * FROM tr_projek");
        $rows = $query->getResult();
        foreach ($rows as $projek) {
            echo $projek->tajuk . '<hr>';
        }
    }

    public function projek() {
        $where = '';
        if ($this->request->getMethod() == 'post') {
            // on click search button
            $data = $this->request->getPost();
            $tahun_dari = $data['tahun_dari'];
            $tahun_hingga = $data['tahun_hingga'];
            //var_dump($data);exit;
            $where = " AND YEAR(tarikh_mula) BETWEEN $tahun_dari AND $tahun_hingga";
        }

        $db = db_connect();
        $sql = "SELECT keterangan_jabatan, COUNT(*) AS bil
                FROM tr_projek p, lt_jabatan j
                WHERE p.kod_jabatan = j.kod_jabatan $where
                GROUP BY keterangan_jabatan
                ORDER BY keterangan_jabatan";
        $query = $db->query($sql);
        $rows = $query->getResult();
        return view('projek', compact('rows'));
    }

    public function projek2() {
        $db = db_connect();
        $sql = "SELECT j.keterangan_status, COUNT(*) AS bil
                FROM tr_projek p, lt_status j
                WHERE p.kod_status = j.kod_status
                GROUP BY p.kod_status, j.keterangan_status";
        $query = $db->query($sql);
        $rows = $query->getResult();
        return view('projek2', compact('rows'));
    }

    public function projek3() {
        $db = db_connect();
        // Jabatan
        $sql = "SELECT * FROM lt_jabatan";
        $query = $db->query($sql);
        $jabatan = $query->getResult();
        // status
        $sql = "SELECT * FROM lt_kategori";
        $query = $db->query($sql);
        $kategori = $query->getResult();
        $status = [];
        foreach ($kategori as $kat) {
            $arr = [];
            $sql = "SELECT j.kod_jabatan, COUNT(*) AS bil
                    FROM lt_jabatan j
                    LEFT JOIN tr_projek p ON j.kod_jabatan = p.kod_jabatan
                    WHERE p.kod_kategori = '{$kat->kod_kategori}'
                    GROUP BY j.kod_jabatan";
            $query2 = $db->query($sql);
            $result = $query2->getResult();
            $i = 0;
            foreach($jabatan as $jab) {
                $arr[$i] = 0;
                foreach ($result as $rs) {
                    if ($rs->kod_jabatan == $jab->kod_jabatan) {
                        $arr[$i] = $rs->bil;
                    }
                }
                $i++;
            }
            $status[$kat->keterangan_kategori] = $arr;
        }
        //dd($status);
        return view('projek3', compact('jabatan', 'status', 'kategori'));
    }



    public function projek4() {
        $db = db_connect();
        $sql = "SELECT p.kod_jabatan, j.keterangan_jabatan, COUNT(*) AS bil 
                FROM tr_projek p, lt_jabatan j
                WHERE p.kod_jabatan = j.kod_jabatan AND p.kod_status = '01'
                GROUP BY p.kod_jabatan, j.keterangan_jabatan";
        $query = $db->query($sql);
        $rows = $query->getResult();
        return view('projek4', compact('rows'));
    }

    public function projek3Data($kod_jabatan, $kod_kategori) {
        $db = db_connect();
        $sql = "SELECT * FROM tr_projek 
                WHERE kod_jabatan = '$kod_jabatan'
                AND kod_kategori = '$kod_kategori'";
        $query = $db->query($sql);
        $rows = $query->getResult();
        return view('projek4_data', compact('rows'));
    }

    public function projek4Data($kod_jabatan) {
        $db = db_connect();
        $sql = "SELECT * FROM tr_projek 
                WHERE kod_jabatan = '$kod_jabatan'
                AND kod_status = '01'";
        $query = $db->query($sql);
        $rows = $query->getResult();
        return view('projek4_data', compact('rows'));
    }
}