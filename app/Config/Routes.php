<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

/*
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
// The Auto Routing (Legacy) is very dangerous. It is easy to create vulnerable apps
// where controller filters or CSRF protection are bypassed.
// If you don't want to define all routes, please use the Auto Routing (Improved).
// Set `$autoRoutesImproved` to true in `app/Config/Feature.php` and set the following to true.
// $routes->setAutoRoute(false);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');
//http://listra.test/hello
//http://localhost/lisstra/public/index.php/hello
$routes->get('/hello', 'SampleController::hello');
$routes->get('/chart1', 'SampleController::chart1');
$routes->get('/chart2', 'SampleController::chart2');
$routes->get('/chart3', 'SampleController::chart3');
$routes->get('/chart4', 'SampleController::chart4');
$routes->match(['get', 'post'], '/projek', 'SampleController::projek');
$routes->get('/projek2', 'SampleController::projek2');
$routes->get('/projek3', 'SampleController::projek3');
$routes->get('/projek3-data/(:any)/(:any)', 'SampleController::projek3Data/$1/$2');
$routes->get('/projek4', 'SampleController::projek4');
$routes->get('/projek4-data/(:any)', 'SampleController::projek4Data/$1');
$routes->get('/db1', 'SampleController::db1');

$routes->get('/pdf1', 'ReportController::html2pdf');
$routes->match(['get', 'post'], '/report-jti', 'ReportController::jti');
$routes->get('/report-jti-pdf', 'ReportController::jtiPdf');
$routes->get('/report-jti-excel', 'ReportController::jtiExcel');
$routes->get('/hello-excel', 'ReportController::helloExcel');


// test upload
$routes->get('/test/upload', 'TestController::index');
$routes->post('/test/upload', 'TestController::upload');

// Laporan JPICT
$routes->match(['get', 'post'], '/jpict-cari', 'JPICTController::cari');
$routes->get('/jpict-pdf', 'JPICTController::genPdf');
$routes->get('/jpict-excel', 'JPICTController::genExcel');
$routes->get('/jpict-mesyuarat/(:num)', 'JPICTController::mesyuarat/$1');

// upload document
$routes->get('/upload', 'UploadController::index');
$routes->get('/download', 'UploadController::getFile');
$routes->post('/upload/(:any)', 
'UploadController::store/$1');
/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (is_file(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
